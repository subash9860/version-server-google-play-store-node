#Version Server API


## Get App Detail only for mofin
### App Id of those app should contain "com.mofin" 
```
    /api/app/list?name=Hamro Smart Banking,facebook
```

## Get List of App of This Name 
```
    /api/app?name=HamroSmartBanking
```

## Get App Version and other detail with their app id
```
    /api/android/detail?appId=com.mofin.uranus.hamro_mofin
```
