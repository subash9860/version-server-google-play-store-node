const Express = require('express')
const gplay = require('google-play-scraper')
const store = require('app-store-scraper')
const path = require('path')
const router = Express.Router()


// for Andriod
// end point http://localhost:3000/api/app/list?name=Hamro Smart Banking,facebook
router.get('/app/list', async function (req, res, next) {
  const names = req.query.name.split(',');
  let responseData = [];
  let requests = names.reduce((promiseChain, name) => {
    return promiseChain.then(() => new Promise((resolve) => {
      gplay
        .search({ term: name, num: 10 })
        .then(async (v) => {
          let app = v.find((e) => e.appId.startsWith('com.mofin'));
          if (typeof app !== 'undefined') {
            await gplay
              .app({ appId: app.appId })
              .then((v) => {
                resolve(responseData.push(v));
              });
          } else {
            resolve({ "errroe": "no data" });
          }
        });
    }));
  },
    Promise.resolve());
  requests.then(() => {
    res.send({ "data": responseData });
  })
    .catch(next);

});


// end point http://localhost:3000/api/app?name=Hamro Smart Banking
router.get('/app', function (req, res, next) {
  const name = req.query.name;
  gplay
    .search({ term: name, num: 100 })
    .then(v => {
      res.send(v);
    })
    .catch(next);
});

// For Android
// end point http://localhost:3000/api/android/detail?appId=com.mofin.uranus.hamro_mofin
router.get('/android/detail', function (req, res, next) {
  const id = req.query.appId
  gplay
    .app({ appId: id })
    .then(v => {
      res.send(v);
    })
    .catch(next);
})


// For ios
// end point http://localhost:3000/api/ios/list?name=Hamro Smart Banking,facebook
router.get('/ios/list', async function (req, res, next) {
  const names = req.query.name.split(',');
  let responseData = [];
  let requests = names.reduce((promiseChain, name) => {
    return promiseChain.then(() => new Promise((resolve) => {
      store
        .search({ term: name, num: 10 })
        .then(async (v) => {
          let app = v.find((e) => e.appId.startsWith('com.mofin'));
          if (typeof app !== 'undefined') {
            await store
              .app({ appId: app.appId })
              .then((v) => {
                resolve(responseData.push(v));
              });
          } else {
            resolve({ "errroe": "no data" });
          }
        });
    }));
  },
    Promise.resolve());

  requests.then(() => {
    res.send({ "data": responseData });
  })
    .catch(next);

});


// end point http://localhost:3000/api/ios?name=Hamro Smart Banking
router.get('/ios', function (req, res, next) {
  const name = req.query.name;
  store
    .search({ term: name, num: 100 })
    .then(v => {
      res.send(v);
    })
    .catch(next);
});


// end point http://localhost:3000/api/ios/detail?appId=com.mofin.uranus.hamro_mofin
router.get('/ios/detail', function (req, res, next) {
  const id = req.query.appId
  store
    .app({ appId: id })
    .then(v => {
      res.send(v);
    })
    .catch(next);
})

// error
function errorHandler(err, req, res, next) {
  res.status(400).json({ message: err.message })
  next()
}

router.use(errorHandler)
module.exports = router
